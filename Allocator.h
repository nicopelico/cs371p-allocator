// ------------------------------
// projects/allocator/Allocator.h
// Copyright (C) 2013
// Glenn P. Downing
// ------------------------------



//g++ -pedantic -std=c++0x -Wall Allocator.h testing.c++ -o allocator
#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert> // assert
#include <cstddef> // ptrdiff_t, size_t
#include <new>     // new

#include <cstring>
#include <iostream>
#include <cmath>

// ---------
// Allocator
// ---------

template <typename T, int N>
class Allocator {
    public:
        // --------
        // typedefs
        // --------

        typedef T                 value_type;

        typedef std::size_t       size_type;
        typedef std::ptrdiff_t    difference_type;

        typedef       value_type*       pointer;
        typedef const value_type* const_pointer;

        typedef       value_type&       reference;
        typedef const value_type& const_reference;

    public:
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const Allocator&, const Allocator&) {
            return true;}                                              // this is correct

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const Allocator& lhs, const Allocator& rhs) {
            return !(lhs == rhs);}

    private:
        // ----
        // data
        // ----

        char a[N];
		
		//int freeBlocks = N - 2*sizeof(int);
		int freeBlocks;
        // -----
        // valid
        // -----

        /**
         * O(1) in space
         * O(n) in time
         * This method checks each pair of sentinels in the heap to insure that they are valid.
		 * @return returns true if valid and false otherwise
         */
        bool valid () const {				//there should be a const here .... bool valid () const { .... need to make this function work when const
		using namespace std;
			bool valid = true;
			int temp = 0;
			int difference = 0;
			int nextPos = 0;
			while(valid){
				temp = view(nextPos);
				difference = temp;
				if(temp < 0)
					difference = -temp;
				nextPos += sizeof(int) + difference;
				if(temp != view(nextPos))
					return false;
				//testing if at last sentinel
				if(nextPos == N - sizeof(int))
					return true;
				nextPos += sizeof(int);
			}
            return false;}
			
		/**
		* This method returns the value pointed to by a char* as an int*
		* @param reference to char
		* @return returns a reference to an integer.
		*/
		int& view (char& c) {
			return *reinterpret_cast<int*>(&c);
		}
		
		/**
		* This method updates the sentinels during a call to allocate.
		* @param new_sent : reference to the new sentinel value
		* @param old_sent_pos : reference to the old sentinel position. 
		*/
		//updates the sentinels for allocate()
		void updateSentinels(int& new_sent, int& old_sent_pos){
			int newSent = new_sent;
			int newSentNeg = -newSent;
			int oldSent = view(a[old_sent_pos]);
			int oldSentPos = old_sent_pos;
			int pos = oldSentPos;
			memcpy(a + pos, &newSentNeg,  sizeof(int));
			pos += sizeof(int) + newSent;
			memcpy(a + pos, &newSentNeg, sizeof(int));
			pos += sizeof(int);
			int updatedSent = oldSent - newSent - 2*sizeof(int);
			if(updatedSent > 0){
				memcpy(a + pos, &updatedSent, sizeof(int));
				pos += sizeof(int) + updatedSent;
				memcpy(a + pos, &updatedSent, sizeof(int));
			}
		}

    public:
		
        // ------------
        // constructors
        // ------------

        /**
         * O(1) in space
         * O(1) in time
         * Sets the initial sentinels of the heap. Sentinel values are total bytes - 8 (for sentinels)
         */
        Allocator () {
		using namespace std;
			freeBlocks = N - 2*sizeof(int);
			int sentinel = N - 8;
			memcpy(a, &sentinel, sizeof(int));
			memcpy(a + (N - sizeof(int)), &sentinel, sizeof(int)); 
			assert(freeBlocks >= (sizeof(T)));		//make sure at least one T can be allocated
            assert(valid());
			}

        // Default copy, destructor, and copy assignment
        // Allocator  (const Allocator&);
        // ~Allocator ();
        // Allocator& operator = (const Allocator&);

        // --------
        // allocate
        // --------

        /**
         * O(1) in space
         * O(n) in time
         * After allocation there must be enough space left for a valid block.
         * The smallest allowable block is sizeof(T) + (2 * sizeof(int)) or no room left at all
         * Chooses the first block that fits.
		 * @param n : Method allocates n T's. Block size is n*sizeof(T)
		 * @return returns a pointer to a T starting at the beginning of the allocated block.
         */
        pointer allocate (size_type n) {
		using namespace std;
			//if I cant have at least 1 valid block left
			if((freeBlocks - n*sizeof(T)) < (sizeof(T)) && freeBlocks != n*sizeof(T)){		//if((freeBlocks - n*sizeof(T)) < (sizeof(T) + 2*sizeof(int)) && freeBlocks != n*sizeof(T)){
				throw bad_alloc();
			}
			
			int nextPos = 0;
			int sentinel = 0;
			//looping through mem blocks
			while(nextPos < N - sizeof(int)){
				sentinel = view(a[nextPos]);
				if(sentinel < 0)
					nextPos += sizeof(int) + -view(a[nextPos]) + sizeof(int);
				else{
					if(sentinel >= n*sizeof(T)){  					// && sentinel >= 4*sizeof(T) + 2
						freeBlocks = freeBlocks - 2*sizeof(int) - n*sizeof(T);
						int newSent = n*sizeof(T);
						updateSentinels(newSent, nextPos);
						assert(valid());
						return (pointer) &a[nextPos + sizeof(int)];
					}
					else{
						nextPos += sizeof(int) + view(a[nextPos]) + sizeof(int);
					}
				}
				
			}
            assert(valid());
			throw bad_alloc();		//cant return valid pointer. throw bad_alloc exception
            return 0;}                   // never reached

        // ---------
        // construct
        // ---------

        /**
         * O(1) in space
         * O(1) in time
         * Constucts an instance of type T using the reference v at the location pointed to by p.
		 * @param p : pointer to type T where a copy of v will be constructed.
		 * @param v : what will be copied into location pointed to by p
         */
        void construct (pointer p, const_reference v) {
            new (p) T(v);                               // this is correct and exempt
            assert(valid());}                           // from the prohibition of new

        // ----------
        // deallocate
        // ----------

        /**
         * O(1) in space
         * O(1) in time
         * Deallocates a block of memory by changing the sentinels around the block being deallocated.
         * After deallocation adjacent free blocks must be coalesced.
		 * @param p : pointer to type T
		 * @param size_type : sizeof(T) * size_type is total block size. This value is ignored and the whole block is deallocated.
         */
        void deallocate (pointer p, size_type) {
		using namespace std;
			/////////////////////////////////////////////////////////////////////////////////////////
			//calculating first sentinel and second sentinel of block pointed to by p
			int* x = reinterpret_cast<int*> (p);		//consider adding view method for other types...
			int numTypes = abs(*(x-1));
			int difference = numTypes/sizeof(T);
			int* firstSent = (x-1);
			int* secondSent = reinterpret_cast<int*>(p + difference);
			////////////////////////////////////////////////////////////////////////////////////////////
			
			////////////////////////////////////////////////////////////////////////////////
			//geting sentinels of previous block (valid if exist)
			int* prevBlockSecondSent = firstSent - 1;
			numTypes = abs(*prevBlockSecondSent);
			difference = numTypes;
			char* prevSentAsChar = reinterpret_cast<char*>(prevBlockSecondSent);
			int* prevBlockFirstSent = reinterpret_cast<int*>(prevSentAsChar - difference - sizeof(int));
			////////////////////////////////////////////////////////////////////////////////
			
			////////////////////////////////////////////////////////////////////////////
			//getting sentinels of next block (valid if exist)
			int* nextBlockFirstSent = secondSent + 1;
			numTypes = abs(*nextBlockFirstSent);
			difference = numTypes;				//add to char* instead
			char* nextBlockFirstSentAsChar = reinterpret_cast<char*>(nextBlockFirstSent);
			int* nextBlockSecondSent = reinterpret_cast<int*>(nextBlockFirstSentAsChar + difference + sizeof(int));
			/////////////////////////////////////////////////////////////////////////////////////
			//determine if coalescing needs to take place			if(prevBlockSecondSent < &view(a[0]))
			bool backwards = false;
			bool forwards = false;
			if(prevBlockSecondSent < &view(a[0]) || *prevBlockSecondSent < 0)
				backwards = false;
			else if(*prevBlockSecondSent > 0)
				backwards = true;
			
			if(nextBlockFirstSent > &view(a[N - sizeof(int)]) || *nextBlockFirstSent < 0)
				forwards = false;
			else if(*nextBlockFirstSent > 0)
				forwards = true;
			//coallesce backwards only
			if(backwards && !forwards){
				*prevBlockFirstSent = *prevBlockFirstSent + 2*sizeof(int) + abs(*firstSent);
				*secondSent = *prevBlockFirstSent;
			}
			//coallesce forwards only
			else if(forwards && !backwards){
				*nextBlockSecondSent = *nextBlockSecondSent + 2*sizeof(int) + abs(*secondSent);
				*firstSent = *nextBlockSecondSent;
			}
			//coallesce forwards and backwards
			else if(forwards && backwards){
				*prevBlockFirstSent = *prevBlockFirstSent + 4*sizeof(int) + abs(*firstSent) + *nextBlockFirstSent;
				*nextBlockSecondSent = *prevBlockFirstSent;
			}
			//no coallescing. just flip deallocated block's sentinels to positive
			else{								
				*firstSent = -*firstSent;
				*secondSent = *firstSent;
			}
            assert(valid());}

        // -------
        // destroy
        // -------

        /**
         * O(1) in space
         * O(1) in time
         * Destroys the allocater and destroys any objects contained in it.
		 * @param p : pointer to type T
         */
        void destroy (pointer p) {
            p->~T();               // this is correct
            assert(valid());}
			
			
		 /**
         * O(1) in space
         * O(1) in time
         * Const view method called by valid. 
		 * @param i : integer passed by value
		 * @return returns a constant integer reference to the value at a[i]
         */
        const int& view (int i) const {
            return *reinterpret_cast<const int*>(&a[i]);}};

#endif // Allocator_h