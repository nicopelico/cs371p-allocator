// ------------------------------------
// projects/allocator/TestAllocator.c++
// Copyright (C) 2013
// Glenn P. Downing
// ------------------------------------

/*
To test the program:
    % ls -al /usr/include/gtest/
    ...
    gtest.h
    ...

    % locate libgtest.a
    /usr/lib/libgtest.a

    % locate libpthread.a
    /usr/lib/x86_64-linux-gnu/libpthread.a
    /usr/lib32/libpthread.a

    % locate libgtest_main.a
    /usr/lib/libgtest_main.a

    % g++ -pedantic -std=c++0x -Wall TestAllocator.c++ -o TestAllocator -lgtest -lpthread -lgtest_main

    % valgrind TestAllocator > TestAllocator.out
*/

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator
#include <cassert>		//assert

#include "gtest/gtest.h"

#include "Allocator.h"

// -------------
// TestAllocator
// -------------

template <typename A>
struct TestAllocator : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A                  allocator_type;
    typedef typename A::value_type      value_type;
    typedef typename A::difference_type difference_type;
    typedef typename A::pointer         pointer;};

typedef testing::Types<
            std::allocator<int>,
            std::allocator<double>,
            Allocator<int, 100>,
            Allocator<double, 100>,
			Allocator<int, 1000> >
        my_types;

TYPED_TEST_CASE(TestAllocator, my_types);

TYPED_TEST(TestAllocator, One) {
    typedef typename TestFixture::allocator_type  allocator_type;
    typedef typename TestFixture::value_type      value_type;
    typedef typename TestFixture::difference_type difference_type;
    typedef typename TestFixture::pointer         pointer;

    allocator_type x;
    const difference_type s = 1;
    const value_type      v = 2;
    const pointer         p = x.allocate(s);
    if (p != 0) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);}}

TYPED_TEST(TestAllocator, Ten) {
    typedef typename TestFixture::allocator_type  allocator_type;
    typedef typename TestFixture::value_type      value_type;
    typedef typename TestFixture::difference_type difference_type;
    typedef typename TestFixture::pointer         pointer;

    allocator_type x;
    const difference_type s = 10;
    const value_type      v = 2;
    const pointer         b = x.allocate(s);
    if (b != 0) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;}}
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);}
            x.deallocate(b, s);
            throw;}
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);}
        x.deallocate(b, s);}}

TEST(Allocator, view_1){
	const Allocator<int, 14> x;
	const Allocator<int, 16> y;
	assert(x.view(0) < y.view(0));
}

TEST(Allocator, view_2){
	const Allocator<double, 99> y;
	const Allocator<int, 99> x;
	assert(y.view(0) == x.view(0));
}

TEST(Allocator, view_3){
	const Allocator<int, 100> y;
	const Allocator<double, 100> x;
	assert(y.view(0) == x.view(96));
}

TEST(Allocator, valid_1){
	const Allocator<double, 16> x;
	assert(x.view(0) != -x.view(12));		//valid called in view. testing the assertion in view that valid == true
}

TEST(Allocator, valid_2){
	Allocator<int, 100> x;
	const Allocator<int, 100>& y = x;
	assert(y.view(0) == y.view(96));
	int *p = x.allocate(1);
	x.deallocate(p, 1);
	p = x.allocate(1);
	x.deallocate(p, 1);	
	assert(y.view(0) == y.view(96));		//valid called 4 times. hoping to see same sentinel values as before
}

TEST(Allocator, valid_3){
	Allocator<int, 100> x;
	const Allocator<int, 100>& y = x;
	assert(y.view(0) == y.view(96));
	int *p = x.allocate(1);
	x.deallocate(p, 1);
	p = x.allocate(4);
	x.deallocate(p, 4);	
	p = x.allocate(1);
	x.deallocate(p, 1);
	assert(y.view(0) == y.view(96));		//valid called 6 times. hoping to see same sentinel values as before
}

TEST(Allocator, constructor_1){
	const Allocator<int, 100> x;
	assert(x.view(0) == 92);
	assert(x.view(96) == 92);
}

TEST(Allocator, constructor_2){
	const Allocator<int, 12> x;		//minimum size heap for int
	assert(x.view(0) == 4);			
	assert(x.view(8) == 4);
}

TEST(Allocator, constructor_3){
	const Allocator<double, 16> x;	//minimum size heap for double
	assert(x.view(0) == 8);			//16 - 8 = 8 blocks. Value of 8 in each sentinel
	assert(x.view(12) == 8);
}

TEST(Allocator, allocate_1){
	Allocator<double, 100> x;
	x.allocate(4);
	const Allocator<double, 100>& y = x; 
	assert(y.view(0) == -32);
	assert(y.view(36) == -32);
	assert(y.view(40) == 52);
	assert(y.view(96) == 52);
}

TEST(Allocator, allocate_2){
	Allocator<int, 12> x;
	x.allocate(1);				//allocate the maximum number of integers possible
	const Allocator<int, 12>& y = x;
	assert(y.view(0) == -4);
	assert(y.view(8) == -4);
}

TEST(Allocator, allocate_3){
	Allocator<int, 44> x;
	x.allocate(1);
	x.allocate(2);
	x.allocate(2);
	const Allocator<int, 44>& y = x;
	assert(y.view(0) == -4);		//first block first sentinel
	assert(y.view(12) == -8);		//second block first sentinel
	assert(y.view(28) == -8);		//third block first sentinel
	assert(y.view(40) == -8);		//last block last sentinel
}

TEST(Allocator, allocate_4){
	Allocator<double, 16> x;
	x.allocate(1);					//allocate max possible doubles
	const Allocator<double, 16>& y = x;
	assert(y.view(0) == -8);
	assert(y.view(12) == -8);
}

TEST(Allocator, allocate_5){
	Allocator<int, 24> x;
	x.allocate(1);						//this is an interesting allocate case. if the buffer was one byte smaller, the allocate will fail even though an integer could fit. 
	const Allocator<int, 24>& y = x;	// 			The freeBlocks left after an allocation would be less than 2*sizeof(int) + sizeof(T)
	assert(y.view(0) == -4);
	assert(y.view(12) == 4);
}

TEST(Allocator, deallocate_1){
	Allocator<int, 100> x;
	int* p = x.allocate(4);
	const Allocator<int, 100>& y = x;
	assert(y.view(0) == -16);
	assert(y.view(20) == -16);
	x.deallocate(p, 4);
	assert(y.view(0) == 92);
	assert(y.view(96) == 92);
}
TEST(Allocator, deallocate_2){
	Allocator<int, 100> x;
	int* p = x.allocate(2);
	int* pp = x.allocate(2);
	const Allocator<int, 100>& y = x;
	assert(y.view(0) == -8);
	assert(y.view(16) == -8);
	x.deallocate(pp, 2);			//coalesce forwards only int
	assert(y.view(16) == 76);
	assert(y.view(96) == 76);
}

TEST(Allocator, deallocate_3){
	Allocator<double, 100> x;
	double * p = x.allocate(2);
	double *pp = x.allocate(2);
	const Allocator<double, 100>& y = x;
	assert(y.view(0) == -16);
	assert(y.view(24) == -16);
	x.deallocate(pp, 2);			//coalesce forwards only double
	assert(y.view(24) == 68);
	assert(y.view(96) == 68);
}

TEST(Allocator, deallocate_4){
	Allocator<int, 36> x;
	const Allocator<int, 36>& y = x;
	int* p1 = x.allocate(1);
	int* p2 = x.allocate(1);
	int* p3 = x.allocate(1);	
	x.deallocate(p1, 1);
	x.deallocate(p3, 1);
	assert(y.view(0) == 4);
	assert(y.view(24) == 4);
	assert(y.view(12) == -4);
	x.deallocate(p2, 1);			//coalesce forwards and backwards int
	assert(y.view(0) == 28);
	assert(y.view(32) == 28);
}

TEST(Allocator, deallocate_5){
	Allocator<double, 48> x;
	const Allocator<double, 48>& y = x;
	double* p1 = x.allocate(1);
	double* p2 = x.allocate(1);
	double* p3 = x.allocate(1);	
	x.deallocate(p1, 1);
	x.deallocate(p3, 1);
	assert(y.view(0) == 8);
	assert(y.view(16) == -8);
	assert(y.view(32) == 8);
	x.deallocate(p2, 1);			//coalesce forwards and backwards double
	assert(y.view(0) == 40);
	assert(y.view(44) == 40);
}
		
		

		


	
	