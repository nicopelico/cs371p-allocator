cs371p-allocator
================
This is my implementation of an allocator. This uses an array of chars with sentinels to denote which regions of the buffer are populated and which ones are empty. A positive sentinel means the region is empty and a negative ones mean the region is populated. An allocator constructor takes a datatype and the number of bytes to manage.
